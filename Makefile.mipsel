# project name (generate executable with this name)
TARGET   = iotscan.mipsel

CFLAG_EXTRA = -Wall

LFLAG_EXTRA = -Wall

CCPATH   = $(PWD)/deps/x-tools/mipsel-linux-musl/bin/

CC       = $(PWD)/deps/x-tools/mipsel-linux-musl/bin/mipsel-linux-musl-gcc
# compiling flags here
CFLAGS   = $(CFLAG_EXTRA) -Iincludes -Ideps/build-libssh2/include

STRIP    = deps/x-tools/mipsel-linux-musl/bin/mipsel-linux-musl-strip

LINKER   = $(PWD)/deps/x-tools/mipsel-linux-musl/bin/mipsel-linux-musl-gcc
# linking flags here
LFLAGS   = $(LFLAG_EXTRA) -Iincludes -Ldeps/build-libssh2/lib -lssh2 -Ldeps/build-mbedtls/lib -lmbedcrypto -lmbedtls -static

# change these to proper directories where each file should be
SRCDIR   = src
HDRDIR   = includes
OBJDIR   = obj
BINDIR   = bin

SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(HDRDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

default: clean remove $(OBJECTS) $(BINDIR)/$(TARGET) COPY STRIP
	@echo "Stripped bin/$(TARGET) from $(shell ls -lh bin/$(TARGET)-tmp | cut -d' ' -f5) to $(shell ls -lh bin/$(TARGET) | cut -d' ' -f5)"
	@rm -f bin/$(TARGET)-tmp

COPY:
	@cp bin/$(TARGET) bin/$(TARGET)-tmp

STRIP:
	@$(STRIP) -S --strip-unneeded --remove-section=.note.gnu.gold-version --remove-section=.comment --remove-section=.note --remove-section=.note.gnu.build-id --remove-section=.note.ABI-tag bin/$(TARGET)

$(BINDIR)/$(TARGET): clean remove $(OBJECTS)
	@$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@echo "Linking complete!"

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@export PATH=$(CC):$PATH
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" to "bin/$(TARGET)" successfully!"

prerequisites: download_sources build-mbedtls build-libssh2
	@echo "Prerequisites setup completed!"

download_sources:
	@echo "Downloading sources for prerequisites!"
	@mkdir -p deps/x-tools
	@wget https://bitbucket.org/GregorR/musl-cross/downloads/crossx86-mipsel-linux-musl-0.9.11.tar.xz -O deps/x-tools/crossx86-mipsel-linux-musl-0.9.11.tar.xz
	@wget https://www.libssh2.org/snapshots/libssh2-1.8.1-20170731.tar.gz -O deps/libssh2-1.8.1-20170731.tar.gz
	@wget https://tls.mbed.org/download/mbedtls-2.5.1-apache.tgz -O deps/mbedtls-2.5.1-apache.tgz
	@echo "Decompressing sources for prerequisites!"
	@tar -xf deps/x-tools/crossx86-mipsel-linux-musl-0.9.11.tar.xz -C deps/x-tools/
	@tar -xf deps/libssh2-1.8.1-20170731.tar.gz -C deps/
	@tar -xf deps/mbedtls-2.5.1-apache.tgz -C deps/

build-mbedtls:
	@cd deps/mbedtls-2.5.1 && CC=$(CCPATH)mipsel-linux-musl-gcc CFLAGS=$(CFLAG_EXTRA) LFLAGS=$(LFLAG_EXTRA) make -j4 && DESTDIR=$$PWD/../build-mbedtls make DESTDIR=$$PWD/../build-mbedtls install
	@echo "Compiled libmbedtls successfully!"

clean-mbedtls:
	@rm -rf deps/build-mbedtls && cd deps/mbedtls* && make clean

build-libssh2:
	@cd deps/libssh2-1.8.1-20170731 && CC=$(CCPATH)mipsel-linux-musl-gcc ./configure --prefix=$$PWD/../build-libssh2 --host=mipsel --target=mipsel-linux-musl --disable-shared --enable-static --with-crypto=mbedtls --with-libmbedcrypto-prefix=$$PWD/../build-mbedtls && make -j4 && make install
	@echo "Compiled libssh2 successfully!"

clean-libssh2:
	@rm -rf deps/build-libssh2 && cd deps/libssh2* && make clean

.PHONY: clean
clean: remove
	@rm -f $(OBJECTS)
	@echo "Cleanup complete!"

.PHONY: remove
remove:
	@rm -f $(BINDIR)/$(TARGET)
	@echo "Executable removed!"
