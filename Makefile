# project name (generate executable with this name)
TARGET   = iotscan

CFLAG_EXTRA = -Wall -s -Os -fno-stack-protector -fomit-frame-pointer -ffunction-sections -fdata-sections -Wl,--gc-sections -falign-functions=1 -falign-jumps=1 -falign-loops=1 -fno-unwind-tables -fno-asynchronous-unwind-tables -fno-math-errno -fno-unroll-loops -fmerge-all-constants -fno-ident -fsingle-precision-constant -ffast-math

LFLAG_EXTRA = -Wall -Wl,-z,norelro -Wl,--hash-style=sysv -Wl,--build-id=none

CC       = gcc
# compiling flags here
CFLAGS   = $(CFLAG_EXTRA) -Iincludes -Ideps/build-libssh2/include

LINKER   = gcc
# linking flags here
LFLAGS   = $(LFLAG_EXTRA) -Iincludes -lssh2 -lmbedcrypto -lmbedtls

# change these to proper directories where each file should be
SRCDIR   = src
HDRDIR   = includes
OBJDIR   = obj
BINDIR   = bin

SOURCES  := $(wildcard $(SRCDIR)/*.c)
INCLUDES := $(wildcard $(HDRDIR)/*.h)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

default: clean remove $(OBJECTS) $(BINDIR)/$(TARGET) COPY STRIP
	@echo "Stripped bin/$(TARGET) from $(shell ls -lh bin/$(TARGET)-tmp | cut -d' ' -f5) to $(shell ls -lh bin/$(TARGET) | cut -d' ' -f5)"
	@rm -f bin/$(TARGET)-tmp

COPY:
	@cp bin/$(TARGET) bin/$(TARGET)-tmp

STRIP:
	@strip -S --strip-unneeded --remove-section=.note.gnu.gold-version --remove-section=.comment --remove-section=.note --remove-section=.note.gnu.build-id --remove-section=.note.ABI-tag bin/$(TARGET)

$(BINDIR)/$(TARGET): clean remove $(OBJECTS)
	@$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@echo "Linking complete!"

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@export PATH=$(CC):$PATH
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" to "bin/$(TARGET)" successfully!"

.PHONY: clean
clean: remove
	@rm -f $(OBJECTS)
	@echo "Cleanup complete!"

.PHONY: remove
remove:
	@rm -f $(BINDIR)/$(TARGET)
	@echo "Executable removed!"
